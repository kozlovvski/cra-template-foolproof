# cra-template-foolproof ![version shield](https://img.shields.io/npm/v/cra-template-foolproof) ![pipeline status](https://gitlab.com/kozlovvski/cra-template-foolproof/badges/master/pipeline.svg) ![coverage report](https://gitlab.com/kozlovvski/cra-template-foolproof/badges/master/coverage.svg)

This is my TypeScript template for creating [Create React App](https://github.com/facebook/create-react-app) apps without a headache.

To use this template, add `--template foolproof` when creating a new app.

For example:

```sh
yarn create react-app my-app --template foolproof
```

## Features

### React App Rewired

This template supports [React App Rewired](https://www.npmjs.com/package/react-app-rewired) - you can easily adjust webpack or babel settings without ejecting from CRA via the `config-overrides.js` file. See [React App Rewired](https://www.npmjs.com/package/react-app-rewired) for more info.

### generate-react-cli

[generate-react-cli](https://www.npmjs.com/package/generate-react-cli) allows you to quickly create well-organized components and their files. `package.json` contains a `"g": "generate-react component"` that you can run via:

```sh
yarn g Foo
```

This will create a `Foo` directory in `src/components` directory and inside it `Foo.tsx`, `Foo.module.scss`, `Foo.test.tsx` andd `Foo.story.tsx` files with appropriate naming and dependencies in those files.

You can also run:

```sh
yarn g Foo --type=page
```

to create a page component in `src/pages` directory. It differs from the default component e.g. it will also create a lazy version of the page, which you can directly implement in the router.

Currently you can generate three types of components:

- default components (use no `--type` flag for this)
- pages (via `--type=page` flag)
- layout (via `--type=layout` flag)

For more information, please refer to:

### Automatic propTypes generation

This template includes [babel-plugin-typescript-to-proptypes](https://www.npmjs.com/package/babel-plugin-typescript-to-proptypes) which will generate propTypes for you automatically!. No need to create them manually - simply type interfaces for props in your components and the plugin will handle the rest. This way you can validate your props not only in buildtime, but also at runtime. Check this example:

```tsx
const Button: React.FC<{ color: string }> = ({ color, children }) => (
  <button style={{ color }}>{children}</button>
);

const HomePage: React.FC = () => (
  <div className={styles["Home"]} data-testid="page-Home">
    <h1>Home page</h1>
    <Button color={(3 as unknown) as string}>foo</Button>
  </div>
);
```

In this example TypeScript will not complain, because we made a type assertion, however if we run this code, we will see a console warning:

```
Warning: Failed prop type: Invalid prop `color` of type `number` supplied to `Button`, expected `string`.
```

This example is obviously unrealistic, but propTypes can spare you a lot of headache when using data from some kind of API which might change its contract in the future.

### SCSS modules with variables and mixins

CRA provides the CSS modules functionality out of the box. This template includes `node-sass` package, which allows you to use SCSS modules in your components. I also included `_variables.scss` and `_mixins.scss` files in `src/scss` directory which are imported in every SCSS module generated with CLI. This way you don't need to manually import them every time you want to create a new component. Simply create a component via `yarn g Foo` and a `Foo.module.scss` file with appropriate imports will be created for you.

#### TypeScript support for SCSS modules

This template also includes a `typescript-plugin-css-modules` plugin which creates type definitions for your CSS or SCSS modules. This has a number of benefits, for example ts-server will throw an error if you try use a non-existent className during compilation. It also enhances your IDE intellisense abilities - Visual Studio Code will now be able to autocomplete your classes.

## Sources of knowledge

Check these sites for more info:

- [Getting Started](https://create-react-app.dev/docs/getting-started) – How to create a new app.
- [User Guide](https://create-react-app.dev) – How to develop apps bootstrapped with Create React App.
