# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.8...v0.2.0) (2020-11-13)


### Features

* **redux:** setup redux and redux-thunk ([afd8e3c](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/afd8e3ca02c1c602953d0a8aca04f6931840ee40))
* **redux:** setup redux and redux-thunk ([d39e12f](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/d39e12fbe01ad6e1287e943eb5b7f5b2d6417feb))
* **tests:** add mockRouteChildrenProps function for pages testing ([0d6baee](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/0d6baee8beb0ea381a8860c063beaf5c75109033))


### Bug Fixes

* **tests:** fix broken data-testid in layout component ([4bc5412](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/4bc5412dbe4b73c0210f368ca46fffc102599c4c))
* **tests:** fix broken layout tests, infer wrapper types ([8726147](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/872614758878650882eaac48ec4ae61ce35cdfa9))
* **tests:** use mockRouteChildrenProps in page tests, fix typings ([9a4c3f7](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/9a4c3f7113141a5e4394574968543e4499dfea06))
* **testutils:** change findByTestAttr wrappers props type to explicit any to allow all components ([d0b44e2](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/d0b44e25e8e2617e7a8c7ac6cc5ff5cbde835147))

### [0.1.8](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.7...v0.1.8) (2020-11-07)


### Features

* **tests:** add a test for page template ([6b924ac](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/6b924ac006c5844f0dfd61d063096bee55727dfd))
* **tests:** add component template test ([bb0eab3](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/bb0eab3de578f7683f5ba9d545e4764d21b522ac))
* **tests:** add findByTestAttr test util ([1f8c57b](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/1f8c57bfb36cc0a8fc03495e9c63d38ca59107a8))
* **tests:** expand layout template test file ([00f63d9](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/00f63d9aa6216829cc7dbdf0d31e280523197edb))


### Bug Fixes

* **cli:** disable story file generation for component template ([0520b1a](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/0520b1a9ae9173683224b5898ecfb8b62678b3f0))
* **examples:** match example layout files with new template files ([7f74846](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/7f7484695bccbe3aea300cd75273f2fd1ac379dd))
* **examples:** match example page files with new template files ([7b43ea4](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/7b43ea45de1676c984c23ddb8d77981e7e2e303a))
* **templates:** export component template props interface ([6cba6d6](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/6cba6d6a13bf3f829eac33eb60f5b3bb0bda4ca3))
* **tests:** add missing mount test to component template test file ([80a1b42](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/80a1b42c0b0feae35f43356e440d83552a4ea640))
* **tests:** change test id naming convention ([344d874](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/344d87428b9286238c19bf46616cb22fd18bf4a3))
* **tests:** update findByTestAttr typing ([994682b](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/994682b6f8293e7922d38237115cff7fdec51f8d))

### [0.1.7](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.6...v0.1.7) (2020-11-05)


### Bug Fixes

* **typings:** fix incorrect layout typings ([b43db84](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/b43db847f5137aca3f942a26d8fba28a685b5d15))

### [0.1.6](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.5...v0.1.6) (2020-11-05)


### Features

* **esling:** add precommit linting ([6958d74](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/6958d746769dc313abd5d4c215aa5e3648baa27c))
* **eslint:** add .eslintignore ([f833930](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/f8339306faa8b66a8133910864067297045829b2))
* **eslint:** add eslint config and dependencies ([cf90d6f](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/cf90d6f087aba18fc392e37c29f0cbc5a8147b71))


### Bug Fixes

* **app:** disable web vitals ([7bd9938](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/7bd99389df733e0075ca53549444d05837620373))
* **eslint:** disable unnecessary eslint rules for templates ([e9da6a5](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/e9da6a5db70eef07783cfcdd71fc8a657dcd6a93))
* **eslint:** fix eslint warnings in template ([559a703](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/559a70313f505ba00182e6daaa8cd6671819026f))
* **eslint:** improve eslint config rules ([a3c2db7](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/a3c2db77d2876ae79df270416f655c4a19028c67))
* **eslint:** remove unnecessary .eslintrc file ([739a929](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/739a929eba99fbee948564d3dcc5ec54f0fc7efb))
* **eslint:** remove unnecessary eslint rules for js files ([6dde246](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/6dde24673a813fdc92c105fb6f033e7c8c79d706))
* **npm:** add missing prettier dependency ([f5435e6](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/f5435e6dfe15aafda92db17312cfdf58c82fbef5))
* **prettier:** add CHANGELOG.md to both .prettierignore files ([3fd5c64](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/3fd5c64f075db4255012c80e0ab4736420a7d96d))

### [0.1.5](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.4...v0.1.5) (2020-11-04)

### Features

- **linting:** add lint-staged and husky dependencies and config to the template ([63b46e3](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/63b46e3343cf24e5574d864a099426721cf9b434))
- **prettier:** add .prettierignore to the template ([3ad68cf](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/3ad68cf83bf590fde922286056759170ec664848))
- **prettier:** expand prettier config ([4cd570f](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/4cd570feee0507467e65db7b136c1070d16f983e))

### [0.1.4](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.3...v0.1.4) (2020-11-04)

### Features

- **proptypes:** add automatic propTypes generation from TS types ([838eb2b](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/838eb2b2533f7e1e0929b92904b3a37cf257c2c3))

### Bug Fixes

- **templates:** fix incorrect type assertions ([6d8637a](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/6d8637aecd1302f690ef9c911e35618bb5dc8f6d))

### [0.1.3](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.2...v0.1.3) (2020-11-02)

### Features

- **cli:** add layout component templates ([f1c13c4](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/f1c13c4e8a7c8389b8f1f10d207f71dabce8410b))
- **cli:** expand cli to support layout components ([4dc042d](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/4dc042d5fa221c7cfef09b439c0da776334c723b))
- **routing:** add example routing ([18ffac6](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/18ffac663d60e0e065544924abd34d8ee67460cc))
- **routing:** add react-router-dom support ([ccea0a4](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/ccea0a492d3af3f99cedac8e6bcf939004fc5196))
- **scss:** add scss variables and mixins files ([45a1667](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/45a16678d31ea93ae5924594317d4528d4bf39ef))
- **tests:** add enzyme support ([aaf282d](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/aaf282df9747f8350e7017ec6f4ed66808ff8efe))

### Bug Fixes

- **cli:** fix wrong destination path for layout components ([b27da2c](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/b27da2ca8ff954824a0d60260f044273ff151eca))
- **scss:** fix incorrect variables and mixins imports ([e0dd691](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/e0dd691a8e91d49965f0a27e18a873a665adbd0c))

### [0.1.2](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.1...v0.1.2) (2020-11-01)

### Features

- **cli:** add generate-react-cli with appropriate templates ([0f9a52c](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/0f9a52c86f6f42139a759bf0d88943876dc6a94d))
- **ts:** add typescript configuration ([3d6c1c8](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/3d6c1c80a7de74700497ef436e8e6ecbee4c991d))
- **versioning:** add standard-version and commitizen configuration ([0a1144e](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/0a1144e666e8a3a8d3aaaaa6a771910e8afb186e))
- add prettier config file ([ad171fc](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/ad171fcb6e906b90f4f96df6a7de80a58d90ef67))

### Bug Fixes

- **cli:** add missing generate-react-cli config file ([ce7b5a9](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/ce7b5a9bdcaced557ac1d38ef8efa04f1a43356f))
- add missing config-overrides.js file ([ef0f2fd](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/ef0f2fddd8a07ed4559ef0973cd796604c43b439))
- add missing customiza-cra dependency ([02d0df2](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/02d0df2a7869d0101a469a7a5f3ffb982e937c3b))
- remove a trailing whitespace in gitignore name ([da665f8](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/da665f8b780cdcdfaf9bdc168c9799e065bcc4b1))

### [0.1.1](https://gitlab.com/kozlovvski/cra-template-foolproof/compare/v0.1.0...v0.1.1) (2020-11-01)

### Bug Fixes

- add missing cra-template-typescript files ([5a6afde](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/5a6afde4ad0a720f583953c2236b90779bd733d0))
- remove forward dot in gitignore, to pass CRA tests ([35b7756](https://gitlab.com/kozlovvski/cra-template-foolproof/commit/35b7756e2dffda7b5e4b8fd694320cbd5a2c2455))

## 0.1.0 (2020-11-01)
